==========================
Format Specification Suite
==========================
:uri: cz-ndic_d2-common-v1.1
:format: DATEX II Situation Publication - Common

This repository provides tools and files for given format:

- schema
- sample(s)
- documentation
- test suite
- unified `tox` based interface for related tools

About described format
======================

For all details, see `FORMAT.yaml`.

Using provided tools
====================

For all details, see `tox.rst`.

Changelog
=========

version 1.0.0 

- Innitial format and documentation

version 1.0.1

- revision of the documentation, proofreading, changes to examples

version 1.1.0rc

- added pointByCoordinates to concepts, schema and samples
- added day-time validity selector to concept and schema
- added feedtype to schema and samples
- added OpenLR location to schema

version 1.1.0

- removed pointByCoordinates and pointByMultiCoordinates as a sole location (without linear reference) since it is not supported by NDIC at the moment. Respective samples has been removed.
- removed day-time validity selector to concept and schema
- confirmed feedtype addidtion to schema and samples
- confirmed OpenLR location addition to schema
